import { initializedStore } from '../../store'
import { useSelector  } from 'react-redux'
import WeatherCurrent from './WeatherCurrent'
import axios from 'axios'
import { Container, Grid } from '@material-ui/core'
import WeatherHourly from './WeatherHourly'
import SearchCityInput from './SearchCityInput'
import styles from '../../styles/Weather.module.css'
import getWeatherBackground from '../../utils/getWeatherBackground'
import dynamic from 'next/dynamic'
const DynamicWeatherDaily = dynamic(() => import('./WeatherDaily'), {ssr: false})
const useWeather = () => {
    return useSelector((state) => {
        return state.weather
    })
}

export default function Weather() {
    const { current } = useWeather()
    return (
    <div className={styles.wrapper} style={{backgroundImage: `url('${getWeatherBackground(current.weather[0].description)}')`}}>
        <Container>
            <Grid container className={styles.container} >
                <Grid container item sm={12} justify="center" alignItems="center" className={styles.form}>
                    <SearchCityInput />
                </Grid>
                <Grid item sm={12} container spacing={3} style={{marginBottom: '25px'}}>
                    <Grid item xs={12} sm={6}><WeatherCurrent /></Grid>
                    <Grid item xs={12} sm={6}><WeatherHourly /></Grid>
                </Grid>
                <Grid item sm={12} style={{borderTop: 'solid white 1px;'}}>
                    <DynamicWeatherDaily />
                </Grid>
            </Grid>
        </Container>
    </div>
    )
}

export async function getServerSideProps(ctx) {
    const reduxStore = initializedStore()
    const { dispatch } = reduxStore

    const res = await axios.get('https://openweathermap.org/data/2.5/onecall?lat=-6.2146&lon=106.8451&units=metric&appid=439d4b804bc8187953eb36d2a8c26a02')
    await dispatch({
        type: 'SET_WEATHER_CURRENT',
        data: res.data.current
    })
    await dispatch({
        type: 'SET_WEATHER_DAILY',
        data: res.data.daily
    })
    await dispatch({
        type: 'SET_WEATHER_HOURLY',
        data: res.data.hourly
    })

    return { props: { initialReduxState: reduxStore.getState() }}
}