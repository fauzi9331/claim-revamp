import { useSelector  } from "react-redux"
import { Grid, Box, GridList } from "@material-ui/core"
import moment from "moment"
import getWeatherIcon from "../../utils/getWeatherIcon"
import styles from "../../styles/WeatherDaily.module.css"

const useDaily = () => {
    return useSelector((state) => {
        return state.weather.daily
    })
}

export default function WeatherHourly() {
    const daily = useDaily()
    let isMobile = false
    if(typeof window !== "undefined") {
        console.log(window.innerWidth)
        if(window.innerWidth < 540){
            isMobile = true
        }
    }
    let cols = isMobile ? 3: 7
    console.log(cols)

    return (
        <div className={styles.container}>
            <GridList cols={cols} spacing={1}>
                {daily.map((item, idx) => {
                    if (idx < 7) {
                        return (
                            <Grid key={idx} container direction="column" justifycontent="center">
                                <Box className={styles.temp}>{idx == 0 ? 'TODAY' : moment().add(idx, "d").format("dddd").toUpperCase()}</Box>
                                <Box className={styles.temp}>{moment().add(idx, "d").format('MMMM Do')}</Box>
                                <Box
                                    component="img"
                                    src={getWeatherIcon(item.weather[0].icon, 1)}
                                    style={{ width: "max-content" }}
                                />
                                <Box className={styles.temp}>Min: <span style={{fontWeight: 600}}>{Math.round(item.temp.min)}</span> °C</Box>
                                <Box className={styles.temp}>Max: <span style={{fontWeight: 600}}>{Math.round(item.temp.max)}</span> °C</Box>
                                <Box className={styles.temp}>{item.weather[0].description}</Box>
                            </Grid>
                        )
                    }
                })}
            </GridList>
        </div>
    )
}
