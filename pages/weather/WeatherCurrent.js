import { useSelector, shallowEqual } from "react-redux"
import { Grid, Box } from "@material-ui/core"
import moment from "moment"
import getWeatherIcon from "../../utils/getWeatherIcon"
import styles from "../../styles/WeatherCurrent.module.css"

const useCurrent = () => {
    return useSelector((state) => {
        return state.weather.current
    })
}

const useCity = () => {
    return useSelector((state) => {
        return state.city
    })
}

export default function Weather() {
    const current = useCurrent()
    let city = useCity()
    const currentDate = moment().format("dddd, MMMM Do YYYY")

    return (
        <div className={styles.container}>
            <Grid container spacing={1}>
                <Grid item xs={12} sm={3} >
                    <Box className={styles.city}> {city.name}</Box>
                    <Box
                        component="img"
                        src={getWeatherIcon(current.weather[0].icon)}
                        style={{ width: "max-content" }}
                    />
                    <Box>{current.weather[0].description}</Box>
                </Grid>
                <Grid container item xs={12} sm={5} direction="column">
                    <Box className={styles.date}>{currentDate}</Box>
                    <Box className={styles.temp}>{Math.round(current.temp)} °C</Box>
                    <Box className={styles.feels_like}>Feels like {Math.round(current.feels_like)} °C</Box>
                </Grid>
                <Grid container item xs={12} sm={4} direction="column">
                    <Box className={styles.more_details}>MORE DETAILS:</Box>
                    <Box>Wind speed: {current.wind_speed} m/s</Box>
                    <Box>Wind direction: {current.wind_deg} °</Box>
                    <Box>Air humidity: {current.humidity} %</Box>
                    <Box>Pressure: {current.pressure} mm</Box>
                    <Box>Visibility: {current.visibility} m</Box>
                    <Box>UV index: {current.uvi}</Box>
                    <Box>Cloudiness: {current.clouds} %</Box>
                </Grid>
            </Grid>
        </div>
    )
}
