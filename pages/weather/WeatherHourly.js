import { useSelector } from "react-redux"
import { Grid, Box, Paper } from "@material-ui/core"
import moment from "moment"
import getWeatherIcon from "../../utils/getWeatherIcon"
import styles from "../../styles/WeatherHourly.module.css"

const useHourly = () => {
    return useSelector((state) => {
        return state.weather.hourly
    })
}

export default function WeatherHourly() {
    const hourly = useHourly()

    return (
        <Paper elevation={3} className={styles.paper}>
            <Grid className={styles.container} container spacing={1}>
                {hourly.map((item, idx) => {
                    if (idx < 6) {
                        return (
                            <Grid key={idx} container item xs={2} direction="column" justifycontent="center">
                                <Box
                                    component="img"
                                    src={getWeatherIcon(item.weather[0].icon, 1)}
                                    style={{ width: "max-content" }}
                                />
                                <Box className={styles.temp}>{Math.round(item.temp)} °C</Box>
                                <Box className={styles.temp}>{moment().add(idx, "h").format("HH:mm")}</Box>
                            </Grid>
                        )
                    }
                })}
            </Grid>
        </Paper>
    )
}
