import { useDispatch } from "react-redux"
import axios from "axios"
import { Grid, TextField } from "@material-ui/core"
import styles from "../../styles/SearchCityInput.module.css"
import { Autocomplete } from "@material-ui/lab"
import React from "react"
import { debounce, truncate } from "lodash"

function Loading({ loading }) {
    if(loading) {
        return <img src="/img/icon/oval.svg" alt="next" />
    }
}

export default function SearchCityInput() {
    const dispatch = useDispatch()
    const [loading, setLoading] = React.useState(false)
    const [cityList, setCityList] = React.useState([
        {
            name: "",
            coord: {},
        },
    ])
    const [value, setValue] = React.useState(cityList[0])

    const fetchCityList = async (newInputValue) => {
        setLoading(true)
        let list = await axios.get(`/api/cities/${newInputValue}`)
        setCityList(list.data)
        setLoading(false)
    }

    const handleOnInputChange = debounce(fetchCityList, 1000)

    const handleValueChange = async (event, newValue) => {
        setValue(newValue)
        let res = await axios.get(
            `https://openweathermap.org/data/2.5/onecall?lat=${newValue.coord.lat}&lon=${newValue.coord.lon}&units=metric&appid=439d4b804bc8187953eb36d2a8c26a02`
        )
        dispatch({
            type: "SET_CITY",
            data: newValue,
        })
        dispatch({
            type: "SET_WEATHER_CURRENT",
            data: res.data.current,
        })
        dispatch({
            type: "SET_WEATHER_DAILY",
            data: res.data.daily,
        })
        dispatch({
            type: "SET_WEATHER_HOURLY",
            data: res.data.hourly,
        })
    }

    return (
        <Autocomplete
            onChange={handleValueChange}
            onInputChange={async (event, newInputValue) => {
                handleOnInputChange(newInputValue)
            }}
            options={cityList}
            getOptionLabel={(option) => option.name}
            style={{ width: 300 }}
            renderInput={(params) => {
                return (
                    <Grid container spacing={2}>
                        <Grid item xs={10}>
                            <TextField {...params} className={styles.input} label="Search city" variant="outlined" />
                        </Grid>
                        {
                            Loading({loading})
                        }
                    </Grid>
                )
            }}
        />
    )
}
