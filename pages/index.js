import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { useRouter } from 'next/router'
import Link from 'next/link'

export default function Home() {
  const router = useRouter()
  const handleClick = (e) => {
    e.preventDefault()
    router.push(e.href)
  }
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome
        </h1>

        <div className={styles.grid}>
        <Link href="/weather">
          <a className={styles.card}>
            <h3>Weather App &rarr;</h3>
            <p>Look up the weather by city name</p>
          </a>
        </Link>
          

        </div>
      </main>

    </div>
  )
}
