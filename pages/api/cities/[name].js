// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import list from '../../../src/data/city.list'

export default (req, res) => {
  if (req.method === 'GET') {
    const { name } = req.query
    let json = list.filter((item) => {
      return item.name.toLowerCase().includes(name.toLowerCase())
    })
    res.status(200).json(json)
  // Process a POST request
  } else {
    // Handle any other HTTP method
  }
}
