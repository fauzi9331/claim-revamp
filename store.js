import { useMemo } from "react"
import { createStore, applyMiddleware } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"

let store

const initialState = {
    weather: {
        current: {
            dt: 0,
            sunrise: 0,
            sunset: 0,
            temp: 0,
            feels_like: 0,
            pressure: 0,
            humidity: 0,
            dew_point: 0,
            uvi: 0,
            clouds: 0,
            visibility: 0,
            wind_speed: 0,
            wind_deg: 0,
            weather: [
              {
                id: 0,
                main: "",
                description: "",
                icon: ""
              }
            ]
          },
        daily: [],
        hourly: [],
    },
    city: { id: 1642911, name: "Jakarta", state: "", country: "ID", coord: { lon: 106.845131, lat: -6.21462 } },
    client: {}
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "SET_WEATHER_CURRENT":
            return {
                ...state,
                weather: {
                    ...state.weather,
                    current: action.data,
                },
            }
        case "SET_WEATHER_DAILY":
            return {
                ...state,
                weather: {
                    ...state.weather,
                    daily: action.data,
                },
            }
        case "SET_WEATHER_HOURLY":
            return {
                ...state,
                weather: {
                    ...state.weather,
                    hourly: action.data,
                },
            }
        case "SET_CITY":
            return {
                ...state,
                city: action.data,
            }
        case "SET_CLIENT":
            return {
                ...state,
                client: action.data,
            }
        default:
            return state
    }
}

function initStore(preloadedState = initialState) {
    return createStore(reducer, preloadedState, composeWithDevTools(applyMiddleware()))
}

export const initializedStore = (preloadedState) => {
    let _store = store ?? initStore(preloadedState)

    if (preloadedState && store) {
        _store = initStore({
            ...store.getState(),
            ...preloadedState,
        })
        store = undefined
    }

    if (typeof window === "undefined") return _store
    if (!store) store = _store

    return _store
}

export function useStore(initialState) {
    const store = useMemo(() => initializedStore(initialState, [initialState]))
    return store
}
