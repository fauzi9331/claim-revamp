const backgroundNameList = [
  'thunderstorm',
  'squall',
  'snow',
  'smoke',
  'rain',
  'fog',
  'dust',
  'drizzle',
  'mist',
  'tornado',
  'cloud',
  'clear',
]
export default function getWeatherBackground(name) {
  if(['sand', 'ash'].includes(name)){
    name = 'dust'
  }
  if(['haze'].includes(name)){
    name = 'mist'
  }
  
  let selectedBackground = backgroundNameList.find(item => {
    return name.includes(item)
  })
  return `/img/weather/${selectedBackground}.png`
}