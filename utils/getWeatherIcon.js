export default function getWeatherIcon(icon, size = 2) {
  if(size > 1) {
    return `http://openweathermap.org/img/wn/${icon}@${size}x.png`
  } else {
    return `http://openweathermap.org/img/wn/${icon}.png`
  }
}